# nist_beacon

An Erlang/OTP application which can be used to to obtain entropy from [NIST Randomness Beacon](https://beacon.nist.gov/home).

**This is an incomplete alpha software; do not make much postive security assumptions about it.**

The application uses [semantic versioning 2.0](http://semver.org/).

[erlang.mk](https://erlang.mk/) is used as a build tool.

## Documentation

Run `make docs` and open `doc/index.html`.

## Contributing

Write function specifications. To run Dialyzer, execute `make dialyze`.

No hard line length limit is imposed.

## License

This software is licensed under under [the Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) (the “License”); you may not use this software except in compliance with the License. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
