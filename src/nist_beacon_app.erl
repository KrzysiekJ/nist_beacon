%% @private
-module(nist_beacon_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).

start(_Type, _Args) ->
    nist_beacon_sup:start_link().

stop(_State) ->
    ok.
