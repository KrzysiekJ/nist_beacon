-module(nist_beacon).
-behaviour(gen_server).

%% API.
-export([start_link/0]).
-export([nb_pulse/1]).
-export([pulse/1]).
-export([request/1]).

%% Spawn exports.
-export([handle_pulse/3]).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include_lib("public_key/include/public_key.hrl").
-include_lib("include/beacon.hrl").

%% @type query() = list(string()). List of query-specific path components.
%% For example, to perform a request to <code>https://beacon.nist.gov/beacon/2.0/chain/1/pulse/1515699180000?type=xml</code>,
%% query needs to be <code>["chain", "1", "pulse", "1515699180000"]</code>.
-type query() :: list(string()).

-type result() :: {ok, erlsom:response()} | {error, term()}.

%% @type timestamp() = pos_integer(). POSIX timestamp in miliseconds.
-type timestamp() :: pos_integer().

-type entropy() :: <<_:512>>.

-record(
   state, {
     model :: erlsom:model(),
     certificates=#{} :: #{string() => #'OTPCertificate'{}}}).

-define(ENDPOINT, "https://beacon.nist.gov/beacon/2.0/").

%% @private
-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% gen_server.

%% @private
init([]) ->
    {ok, #state{model=xsd_model()}}.

%% @private
handle_call({request, Args}, _, State) ->
    {reply, fetch_response(Args, State), State};
handle_call({pulse, Timestamp}, From, State) ->
    _ = spawn_monitor(?MODULE, handle_pulse, [Timestamp, From, State]),
    {noreply, State};
handle_call(_Request, _From, State) ->
    {reply, ignored, State}.

%% @private
handle_cast({add_cert, Id, Cert}, State=#state{certificates=Certs}) ->
    {noreply, State#state{certificates=Certs#{Id => Cert}}};
handle_cast(_Msg, State) ->
    {noreply, State}.

%% @private
handle_info(_Info, State) ->
    {noreply, State}.

%% @private
terminate(_Reason, _State) ->
    ok.

%% @private
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% @doc A low-level function used to perform API requests.
-spec request(query()) -> result().
request(Args) ->
    gen_server:call(?MODULE, {request, Args}).

%% @doc Try to obtain random 512 bits either dated at timestamp or next closest to it.
%% This is a non-blocking variant of {@link pulse/1}.
-spec nb_pulse(timestamp()) -> {ok, entropy()} | {error, term()}.
nb_pulse(Timestamp) ->
    gen_server:call(?MODULE, {pulse, Timestamp}).

%% @doc Obtain random 512 bits either dated at timestamp or next closest to it.
%% This function will wait if entropy is not available yet (even in case of connection errors and server errors).
%% Retries will be performed at 60s intervals.
%% For a non-blocking variant, see {@link nb_pulse/1}.
-spec pulse(timestamp()) -> entropy().
pulse(Timestamp) ->
    ok = wait_until(Timestamp),
    case nb_pulse(Timestamp) of
        {ok, Entropy} ->
            Entropy;
        {error, _} ->
            ok = timer:sleep(60000),
            pulse(Timestamp)
    end.

%% @private
-spec handle_pulse(timestamp(), From :: {pid(), Tag :: term()}, #state{}) -> ok | {error, term()}.
handle_pulse(Timestamp, From, State) ->
    Reply =
        case fetch_response(["pulse", "time", integer_to_list(Timestamp)], State) of
            {ok, #response{choice=[Pulse=#pulseType{outputValue=OutputValue}]}} ->
                case verify_pulse(Pulse, State) of
                    ok ->
                        Entropy = hex:hex_to_binary(OutputValue),
                        {ok, Entropy};
                    {error, Reason} ->
                        {error, {verification_failed, Reason}}
                end;
            {ok, #response{choice=#errorType{code=404}}} ->
                {error, pulse_not_available};
            Error={error, _} ->
                Error;
            _ ->
                {error, unknown}
        end,
    gen_server:reply(From, Reply).

-spec wait_until(timestamp()) -> ok.
wait_until(TimestampMs) ->
    Now = os:system_time(millisecond),
    case Now > TimestampMs of
        true ->
            ok;
        false ->
            timer:sleep(TimestampMs - Now)
    end.

-spec fetch_response(query(), #state{}) -> result().
fetch_response(Args, #state{model=Model}) ->
    URL = lists:flatten([?ENDPOINT, lists:join($/, Args), "?type=xml"]),
    case httpc:request(URL) of
        {ok, {{_, StatusCode, _}, _, MaybeXML}} ->
            case StatusCode =:= 200 orelse StatusCode =:= 404 of
                true ->
                    {ok, Response, _} = erlsom:scan(MaybeXML, Model),
                    {ok, Response};
                false ->
                    {error, {unexpected_status_code, StatusCode}}
            end;
        Failure={error, _} ->
            Failure
    end.

-spec xsd_model() -> erlsom:model().
xsd_model() ->
    XSDPath = filename:join(code:priv_dir(nist_beacon), "beacon-2.0.xsd"),
    {ok, Model} = erlsom:compile_xsd_file(XSDPath),
    Model.

-spec verify_pulse(#pulseType{}, #state{}) -> ok | {error, term()}.
verify_pulse(
  Pulse=#pulseType{certificateId=CertId},
  State) ->
    case fetch_certificate(CertId, State) of
        {error, Reason} ->
            {error, {failed_to_fetch_certificate, Reason}};
        Cert ->
            case verify_sig(Pulse, Cert) of
                ok ->
                    ok;
                Error ->
                    Error
            end
    end.

-spec verify_sig(#pulseType{}, #'OTPCertificate'{}) -> ok | {error, term()}.
%% @todo Implement this.
verify_sig(_Pulse, _Cert) ->
    %% A pseudocheck to tame Dialyzer.
    case binary:decode_unsigned(<<1>>) > 0 of
        true ->
            ok;
        false ->
            {error, foo}
    end.

-spec fetch_certificate(string(), #state{}) -> #'OTPCertificate'{} | {error, term()}.
fetch_certificate(Id, #state{certificates=Certs}) ->
    case Certs of
        #{Id := Cert} ->
            Cert;
        _ ->
            URL = lists:flatten([?ENDPOINT, "certificate/", Id]),
            case httpc:request(URL) of
                {ok, {{_, 200, _}, _, CertPEM}} ->
                    [Cert] = public_key:pem_decode(list_to_binary(CertPEM)),
                    ok = gen_server:cast(?MODULE, {add_cert, Id, Cert}),
                    Cert;
                _ ->
                    {error, could_not_fetch_certificate}
            end
    end.
