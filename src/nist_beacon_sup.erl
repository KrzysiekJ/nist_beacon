%% @private
-module(nist_beacon_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
    Procs =
        [#{id => nist_beacon,
           start => {nist_beacon, start_link, []}}],
    {ok, {{one_for_one, 1, 5}, Procs}}.
