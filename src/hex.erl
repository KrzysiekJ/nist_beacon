%% @private
-module(hex).

-export([hex_to_binary/1]).

-spec hex_to_binary(string()) -> binary().
hex_to_binary(Hex) ->
    ByteLength = length(Hex) div 2,
    Int = hex_to_integer(Hex),
    <<Int:ByteLength/unit:8>>.

-spec hex_to_integer(string()) -> integer().
hex_to_integer(Hex) ->
    hex_to_integer(Hex, 0).

-spec hex_to_integer(string(), integer()) -> integer().
hex_to_integer([], Acc) ->
    Acc;
hex_to_integer([X|Tail], Acc) ->
    hex_to_integer(Tail, int(X) + 16 * Acc).

-spec int(integer()) -> integer().
int(C) when $0 =< C, C =< $9 ->
    C - $0;
int(C) when $A =< C, C =< $F ->
    C - $A + 10;
int(C) when $a =< C, C =< $f ->
    C - $a + 10.
