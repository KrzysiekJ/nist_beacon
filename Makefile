PROJECT = nist_beacon
PROJECT_DESCRIPTION = An application used to obtain entropy from NIST Randomness Beacon
PROJECT_VERSION = 0.2.1

DEPS = erlsom
dep_erlsom = git https://github.com/willemdj/erlsom.git df4526ce3c50ea27a4d37f4be3582037368f8ce7

LOCAL_DEPS = inets ssl

SP = 4

include erlang.mk

$(PROJECT).d:: include/beacon.hrl

include/beacon.hrl: priv/beacon-2.0.xsd
	$(verbose) mkdir -p include
	$(gen_verbose) $(SHELL_ERL) -pa $(SHELL_PATHS) -noshell -s ssl -eval "\
		application:ensure_all_started(erlsom),\
		erlsom:write_xsd_hrl_file(\"priv/beacon-2.0.xsd\", \"include/beacon.hrl\"),\
		halt()"

clean::
	$(verbose) rm -f include/beacon.hrl
